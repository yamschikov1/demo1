import supertest from 'supertest';
import { urls } from '../framework/config/index';

function buildRequest() {
        const r = supertest(urls.vikunja);
        return r;
}

export {
    buildRequest,
}
