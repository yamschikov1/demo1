import { Severity } from "jest-allure/dist/Reporter";
import { test } from '@jest/globals';

function sum(a, b) {
  return a + b;
}

describe('Describe', () => {
  test('adds 1 + 2 to equal 3', async () => {
    expect(sum(1, 2)).toBe(3);
    reporter
      .description("Feature should work cool")
      .severity(Severity.Critical)
      .feature('Feature.Betting')
      .story("BOND-007");
    reporter.startStep("Check it's fancy");
    // expect that it's fancy
    reporter.endStep();
  });


});
